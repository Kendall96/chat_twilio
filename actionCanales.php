<?php 
    require_once "servicios_twilio.php";
    
    if($_SERVER["REQUEST_METHOD"] == "POST"){
        $accion = $_POST["accion"];

        if($accion === "create"){
            $nombreChat = $_POST["nameChat"];
            CrearCanales($nombreChat);
            header("refresh:1;index.php");
        }
        elseif($accion === "delete"){
            $sidChannel = $_POST["idChannel"]; 
            eliminarCanales($sidChannel);
            header("refresh:1;index.php");
        }
        elseif($accion === "update"){
            $sidChannel = $_POST["idChannel"];
            $nameChannel = $_POST["nameChat"];
            modificarCanales($sidChannel, $nameChannel);
            header("refresh:1;index.php");
        }
        elseif($accion === "addUser"){
            $sidChannel = $_POST["idChannel"];
            $nickname = $_POST["nameUser"];
            $nameChannel = $_POST["nameChannel"];
            
            $user = addMembers($sidChannel, $nickname);
            if($user != ""){
                header("Location: infoChannel.php?sidChannel=$sidChannel&nameChannel=$nameChannel&nameUser=$user");
            }else{
                header("Location: index.php");
            }
            
        }
        elseif($accion === "listUser"){
            $sidChannel = $_POST['idChannel'];
            $user1 = $_POST['user'];
            $usuarios = obtenerMiembros($sidChannel);

            foreach ($usuarios as $record) {
                $nombre = $record->identity;
                
                if($nombre == $user1){
                    echo "
                        <tr>
                            <td>$nombre</td>
                            <td><i class='far fa-check-circle'></i></td>
                        </tr>
                    ";
                }else{
                    echo "
                        <tr>
                            <td>$nombre</td>
                        </tr>
                    ";
                }
            };  
        }
        elseif($accion === "listMensage"){
            $sidChannel = $_POST['idChannel'];
            //$who = $_POST['who'];
            $mensages = obtenerMensages($sidChannel);

            foreach ($mensages as $record) {
                $nombreUser = $record->from;
                $body = $record->body;
                $fecha = $record->dateCreated;

                $date = $fecha->format('Y-m-d H:i:s');
                echo "
                <li style='width:100%'>
                    <div class='msj macro'>
                        <div class='avatar'> <p style='width:100%;'>$nombreUser</p> </div>
                        <div class='text text-l'>
                            <p>$body</p>
                            <p><small>$date</small></p>
                        </div>
                    </div>
                </li>
                ";
            };  
        }
    }
?>