var me = {};
me.avatar = "https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48";

var you = {};
you.avatar = "https://a11.t26.net/taringa/avatares/9/1/2/F/7/8/Demon_King1/48x48_5C5.jpg";
           
//esta funcion agrega los mensajes a el cuerpo del chat
function getMensages(idChannel){
    let time = 0;
    let accion = 'listMensage';

    $.ajax({
        type: "POST",
        url: "actionCanales.php",
        data: {accion, idChannel},
        success: function(data){
            setTimeout(
                function(){                        
                    $("ul").html(data).scrollTop($("ul").prop('scrollHeight'));
                }, time);
        }
    });
}

//esta funcion manda la informacion del mensaje para guardarlo en twilio  
function insertChat(text, nameuser, idChannel){
    console.log(nameuser + " esta la funcion insert. ");
    $.ajax({
        type: "POST",
        url: "mensajes.php",
        data: { nameuser, idChannel, text},
        success: function(){
            
        }
    });

    getMensages(idChannel);
}

function resetChat(){
    $("ul").empty();
}

//este funcion es para cuando se preciona el boton de enviar el mensaje manda a llamar la funcion de insertChat
function enviarMensaje(){
    let text = jQuery('#mytext').val();
    let idChannel = jQuery('#idChannel').val();
    let nameuser = jQuery('#user').val();
    console.log(text, nameuser);
    if (text !== ""){
        insertChat(text, nameuser, idChannel);              
        $("#mytext").val('');
    }
}

//este funcion es para cuando se preciona el enter del teclado para enviar el mensaje manda a llamar la funcion de insertChat
$(".mytext").on("keydown", function(e){
    if (e.which == 13){
        let text = $(this).val();
        let idChannel = jQuery('#idChannel').val();
        let nameuser = jQuery('#user').val();
        console.log(nameuser + " esta es cuando se le da enter. ");
        if (text !== ""){
            insertChat(text, nameuser, idChannel);              
            $(this).val('');
        }
    }
});

//esta envento click es para cuando de preciona la tecla enter
$('#submit1').click(function(){
    $(".mytext").trigger({type: 'keydown', which: 13, keyCode: 13});
})

//esta envento click es para cuando de preciona el boton de enviar
jQuery('#submit1').bind('click', function (element, event) {
    enviarMensaje();
});

//-- Clear Chat
resetChat();
