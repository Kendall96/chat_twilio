<?php
    require "servicios_twilio.php";

    $sid= $_GET['sidChannel'];
    $name = $_GET['nameChannel'];
    $nameUser = $_GET['nameUser'];
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel=StyleSheet href="styless.css" TYPE="text/css" media=screen>
    <link rel=StyleSheet href="chatmsg.css" TYPE="text/css" media=screen>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <script>
        function listaUsuarios() {                             
            let idChannel = '<?php echo $sid; ?>';
            let accion = 'listUser';
            let user = '<?php echo $nameUser; ?>';
            console.log('idChannel:', '<?php echo $sid; ?>');
            //esta funcion lo que hace es pasar la informacion a el archivo
            //actionCanales.php el cual se encarga de obtener la infomacion
            //y procesarla para que luego en la respuesta del Success se pueda
            //incorporar en el body de la tabla3. 
            $.ajax({
                type: "POST",
                url: "actionCanales.php",
                data: { 
                    idChannel:idChannel,
                    accion:accion,
                    user:user 
                },
                success: function(data){
                    $("#tablita").html(data);
                }
            });
        };
    </script>
</head>
<body onload="listaUsuarios();">

    <header class="header"> 
        <a href="index.php" id="regreso" type="button" class="btn btn-primary"><i class="fas fa-arrow-left"> </i>  Regresar</a>
    </header> 

    <div id="indentificacion">
        <p style="margin-top: 13px; color: white;"><i class="far fa-user"> </i> <?php echo $nameUser; ?></p>
    </div> 

    <div class="titulo"> 
        <p>Chat</p>
    </div>
    <div class="col-sm-3 col-sm-offset-4 frame">
        <ul></ul>
        <div>
            <div class="msj-rta macro">                        
                <div class="text text-r" style="background:whitesmoke !important">
                    <input id="mytext" class="mytext" placeholder="Type a message"/>
                    <input id="idChannel" type='hidden' name='idChannel' value="<?php echo $sid;?>">
                    <input id="user" type='hidden' name='user' value="<?php echo $nameUser;?>">
                </div> 

            </div>
            <div style="padding:10px;">
                <button type="button" id="submit1" class="btn btn-outline-primary"><i class="fas fa-location-arrow"></i></button>
            </div>                
        </div>
    </div>      

    <div>
        <!--tabla usuarios-->
        <table id="table4" class="table">
            <thead>
                <tr style="background-color: rgb(52, 152, 219); color: white; text-align: center;">
                    <th  scope="col">Canal</th>
                </tr>
            </thead>
            <tbody >
                <tr>
                    <td><?php echo $name; ?></td>
                </tr>

            </tbody>
        </table>
    </div>

    <div>
        <table id="table3" class="table">
            <thead>
                <tr style="background-color: rgb(52, 152, 219); color: white; text-align: center;">
                    <th scope="col"><i class="far fa-user"> </i> Usuarios</th>
                    <th scope="col">></th>
                </tr>
            </thead>
            <tbody id="tablita">

            </tbody>
        </table> 
    </div> 

    <!--este timer esta cada segundo llamando la funcion de getMensages para refrescar los mensages del chat-->
    <script>$(document).ready(function () { 
        setInterval(function() { 
            getMensages('<?php echo $sid; ?>'); }, 1000); //5 seconds
        });
    </script>

    <!--este timer esta cada segundo llamando la funcion de listarUsuarios para refrescar los usuarios que se registran-->
    <script>$(document).ready(function () { 
        setInterval(function() { 
            listaUsuarios(); }, 1000); //5 seconds
        });
    </script>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="chat.js"></script>
    <script>
        getMensages('<?php echo $sid; ?>');
    </script>
</body>
</html>