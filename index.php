<?php
    session_start();
    require "servicios_twilio.php";
    
    if(empty(listarServicios())){
        crearServicios();
    }
    //addMembers();


    if($_SERVER["REQUEST_METHOD"] == "POST"){

        //$user = $_POST["user"];
        //$pass = $_POST["password"];

        if(!empty($_POST["user"]) != "" && !empty($_POST["password"]) != "" ){
            if($_POST["user"] === "kendall" && $_POST["password"] === "123" ){
                //session_start();
                $_SESSION["admin"] = "kendall";
                $_SESSION["pass"] = "123";
    
            }
        }
    }
    
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chat</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel=StyleSheet href="styless.css" TYPE="text/css" media=screen>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
</head>
<body>

    <?php if(!empty($_SESSION["admin"]) == "kendall" && !empty($_SESSION["admin"]) == "123"): ?>
    <header class="header"> 
        <a href="Sign_Up.php" id="signUp" type="button" class="btn btn-danger">Sign Up  <i class="fas fa-sign-in-alt"></i></a>
    </header>

    <button id="btn1" type="button" class="btn btn-primary" data-toggle="modal" data-target="#createChannel"><i class="far fa-plus-square"></i> Canal</button>
    
    <table id="table" class="table table-striped">
        <thead>
            <tr style="background-color: rgb(52, 152, 219); color: white;">
                <th scope="col">Nombre Canal</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <?php 
        //lista los canales y ademas se asignan dos botones uno para Update y otro para Delete
        $canales = listarCanales();
        foreach($canales as $record) { ?>
            <tbody>
                <tr>
                    <td> <?php echo $record->friendlyName ?> </td>
                    <!-- Modal update Channel -->
                    <td>
                        <a href='#' class='btn btn-success' data-toggle='modal' data-target='#sid<?php echo $record->sid;?>'><i class="far fa-edit"></i></a>
                        <div class='modal fade' role='dialog' aria-hidden='true' id='sid<?php echo $record->sid;?>'>
                            <div class='modal-dialog'>
                                <div class='modal-content'>
                                    <div class='modal-header'>
                                        <h4 id="titulos" class='modal-title'>Editar Canal</h4>
                                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>
                                    </div>
                                    <div class='modal-body'>
                                        <form action= "actionCanales.php" method="POST">
                                            <div class="form-row">
                                                <div class="form-group col-md-12">
                                                    <label for="nameChat">Nombre Canal</label>
                                                    <input name="nameChat" type="nameChat" class="form-control" id="nameChat" value="<?php echo $record->friendlyName; ?>">
                                                    <input type='hidden' name='accion' value="update">
                                                    <input type='hidden' name='idChannel' value="<?php echo $record->sid;?>">
                                                </div>
                                            </div>
                                            <button style="margin-left: 80%;" type='submit' class='btn btn-primary'>Actualizar</button>
                                        </form>
                                    </div>
                                    <div class='modal-footer'>                                      
                                        <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </td>
                    <!-- delete Channel -->
                    <td style='text-align: center;'>
                        <form action="actionCanales.php" method="POST">
                            <div>
                                <input type="hidden" name="idChannel" value="<?php echo $record->sid;?>">
                                <input type="hidden" name="accion" value="delete">
                            </div>
                            <button type="submit" id="btnDelete" class='btn btn-danger'><i class="fas fa-trash-alt"></i></button>
                        </form>
                    </th>
                </tr>
            </tbody>
        <?php } ?>
    </table>

    <?php else : ?>
    <!--Esta parte es para los usuarios corrientes-->

    <header class="header">  
        <button id="btn2" type="button" class="btn btn-primary" data-toggle="modal" data-target="#LoginAdmin"><i class="fas fa-sign-in-alt">  </i> LOGIN</button>
        
        <!-- Modal Login Admin -->
        <div class="modal fade" id="LoginAdmin" tabindex="-1" role="dialog" aria-labelledby="LoginAdminLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 id="titulos" class="modal-title" id="LoginAdminLabel">Login</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action= "index.php" method="post">
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-user"></i></div>
                                </div>
                                <input name="user" type="text" class="form-control" id="user" placeholder="User" required>
                            </div>
                            <div class="input-group mb-2">
                                <div class="input-group-prepend">
                                    <div class="input-group-text"><i class="fas fa-key"></i></div>
                                </div>
                                <input name="password" type="password" class="form-control" id="password" placeholder="Password" required>
                            </div>
                        </div>
                    </div>
                    <button style="margin-left: 86%;" type='submit' class='btn btn-primary'>Sign in</button>
                </form>
            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </header>
    
    <div class="Container" sytyle="display: flex;">
        <div> 
            <table id="table2" class="table table-striped">
                    <thead>
                        <tr style="background-color: rgb(52, 152, 219); color: white;">
                            <th style="text-align: center;" scope="col"><i class="fab fa-snapchat"> </i> Nombre Canal</th>
                        </tr>
                    </thead>
                    <?php 
                    //lista los canales y ademas se asignan dos botones uno para Update y otro para Delete
                    $canales = listarCanales();
                    $cont = 0;
                    foreach($canales as $record) { 
                        $cont += 1;             ?>
                        <tbody>
                            <tr>
                                <td style="text-align: center;"> <!--actionCanales.php-->
                                    <a href='#' style="width: 70%; color: white;" class='btn btn-outline-primary' data-toggle='modal' data-target='#sid<?php echo $record->sid;?>'><?php echo $record->friendlyName ?></a>
                                    <div class='modal fade' role='dialog' aria-hidden='true' id='sid<?php echo $record->sid;?>'>
                                        <div class='modal-dialog'>
                                            <div class='modal-content'>
                                                <div class='modal-header'>
                                                    <h4 id="titulos" class='modal-title'>Unirse a Canal <?php echo $record->friendlyName ?></h4>
                                                    <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>
                                                </div>
                                                <div class='modal-body'>
                                                    <form action= "actionCanales.php" method="POST">
                                                        <div class="form-row">
                                                            <div class="form-group col-md-12" style="text-align: left;">
                                                                <label for="nameUser">Your nickname</label>
                                                                <input name="nameUser" type="nameUser" class="form-control" id="nameUser" placeholder="nickname" required>
                                                                <input type='hidden' name='accion' value="addUser">
                                                                <input type='hidden' name='idChannel' value="<?php echo $record->sid;?>">
                                                                <input type='hidden' name='nameChannel' value="<?php echo $record->friendlyName;?>">
                                                            </div>
                                                        </div>
                                                        <button style="margin-left: 80%;" type='submit' class='btn btn-primary'>Unirse</button>
                                                    </form>
                                                </div>
                                                <div class='modal-footer'>                                      
                                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    <?php } ?>
            </table>  
        </div>     
    </div>

    <?php endif; ?>


    <!-- Modal Create Channel -->
    <div class="modal fade" id="createChannel" tabindex="-1" role="dialog" aria-labelledby="createChannelLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
            <h5 id="titulos" class="modal-title" id="createChannelLabel">Crear canal</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form action= "actionCanales.php" method="post">
                <div class="form-row">
                    <div class="form-group col-md-12">
                        <label for="nameChat">Canal</label>
                        <input name="nameChat" type="nameChat" class="form-control" id="nameChat" placeholder="Nombre Canal">
                        <input type='hidden' name='accion' value="create">
                    </div>
                </div>
                <button style="margin-left: 86%;" type='submit' class='btn btn-primary'>Crear</button>
            </form>
        </div>
        <div class="modal-footer">
            <button type='button' class='btn btn-default' data-dismiss='modal'>Cerrar</button>
        </div>
        </div>
    </div>

    <!--add user to a channel
    <td></td>
    <td>
        <a href='#' class='btn btn-success' data-toggle='modal' data-target='#sid<?php //echo $record->sid;?>'><i class="fas fa-plus-circle"></i></a>
        <div class='modal fade' role='dialog' aria-hidden='true' id='sid<?php //echo $record->sid;?>'>
            <div class='modal-dialog'>
                <div class='modal-content'>
                    <div class='modal-header'>
                        <h4 id="titulos" class='modal-title'>Unirse a Canal <?php //echo $record->friendlyName ?></h4>
                        <button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>×</span></button>
                    </div>
                    <div class='modal-body'>
                        <form action= "actionCanales.php" method="POST">
                            <div class="form-row">
                                <div class="form-group col-md-12">
                                    <label for="nameUser">Your nickname</label>
                                    <input name="nameUser" type="nameUser" class="form-control" id="nameUser" placeholder="nickname">
                                    <input type='hidden' name='accion' value="addUser">
                                    <input type='hidden' name='idChannel' value="<?php //echo $record->sid;?>">
                                </div>
                            </div>
                            <button style="margin-left: 80%;" type='submit' class='btn btn-primary'>Unirse</button>
                        </form>
                    </div>
                    <div class='modal-footer'>                                      
                                        
                    </div>
                </div>
            </div>
        </div>
    </td> -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
</body>
</html>