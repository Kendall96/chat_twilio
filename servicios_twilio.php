<?php

require_once './twilio-php-master/twilio-php-master/Twilio/autoload.php';

use Twilio\Rest\Client;

//<<<< FUNCTIONS FOR SERVICES >>>>

///esta es para crear y obtener el servicio
function listarServicios(){
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $services = $twilio->chat->v2->services
                             ->read();

    foreach ($services as $record) {
        return $record->sid;
    }
}

//esta funcion es para crear un nuevo servicio
function crearServicios(){
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $service = $twilio->chat->v2->services
                                ->create("FRIENDLY_NAME");

}

//<<<< FUNCTIONS FOR CHANNELS >>>>

//esta para listar los canales
function listarCanales(){
    // Find your Account Sid and Auth Token at twilio.com/console
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $channels = $twilio->chat->v2->services(listarServicios())
                                ->channels
                                ->read();
    return $channels;
}

//esta funcion es para crear nuevos canales
function CrearCanales($nombreChat){
    // Find your Account Sid and Auth Token at twilio.com/console
    //$nombreChat = $this->input->post("nameChat");
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $channel = $twilio->chat->v2->services(listarServicios())
                                ->channels
                                ->create(array("friendlyName" => $nombreChat));
}

//esta funcion es para actulizar los canales
function modificarCanales($sidChannel,$nameChannel){
    // Find your Account Sid and Auth Token at twilio.com/console
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $channel = $twilio->chat->v2->services(listarServicios())
                                ->channels($sidChannel)
                                ->update(array(
                                            "friendlyName" => $nameChannel
                                        )
                                );

    //print($channel->friendlyName);
}

//esta funcion es para eliminar canales por medio de su sid.
function eliminarCanales($sidChannel){
    // Find your Account Sid and Auth Token at twilio.com/console
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $twilio->chat->v2->services(listarServicios())
                    ->channels($sidChannel)
                    ->delete();
}

//<<<<< FUNCTIONS FOR MEMBERS >>>>>

function addMembers($sidChannel,$nickname){
    // Find your Account Sid and Auth Token at twilio.com/console
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $validar = false;
    $usuarios = obtenerMiembros($sidChannel);

    foreach ($usuarios as $record) {
        $nombre = $record->identity;

        if($nombre === $nickname){
            $validar = true;
        }
    };

    if($validar){
        return $nickname;
    }
    else{
        $member = $twilio->chat->v2->services(listarServicios())
                            ->channels($sidChannel)
                            ->members
                            ->create($nickname);

        return $member->identity;
    }
}

function obtenerMiembros($sidChannel){
    // Find your Account Sid and Auth Token at twilio.com/console
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $members = $twilio->chat->v2->services(listarServicios())
                                ->channels($sidChannel)
                                ->members
                                ->read();

    return $members;
}

//<<<<< FUNCTIONS FOR MESSAGES >>>>>

//esta funcion es para obtener los mensajes
function obtenerMensages($sidChannel){
    $sid    = "AC971afca8c475e189d26a6c837ae54c44";
    $token  = "b16c026cc33aad91b6804328044d4a3c";
    $twilio = new Client($sid, $token);

    $messages = $twilio->chat->v2->services(listarServicios())
                                ->channels($sidChannel)
                                ->messages
                                ->read();

    return $messages;
}